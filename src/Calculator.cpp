#include "include/Calculator.h"
#include<qthread.h>
#include<QProcess>
#include<QFileDialog>
#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"

Calculator::Calculator(QWidget* parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.m_calculateButton, SIGNAL(clicked()), this, SLOT(Calculate()));
	connect(ui.m_loadInputDataButton, SIGNAL(clicked()), this, SLOT(ReadInputData()));
	connect(ui.m_createFileButton, SIGNAL(clicked()), this, SLOT(CreateBaseDataFile()));
	InitThread();
	m_maxCount = 0;
}


Calculator::~Calculator()
{
	ExitThread();
	if (m_scriptThread)
		delete m_scriptThread;
}

void Calculator::Calculate()//计算结果
{
	if (m_calculateThread.isRunning())
		return;
	if (m_maxCount != m_inputNames.size())
		return;
	if (m_maxCount != m_inputValues.size())
		return;
	QString text = ui.m_inputText->toPlainText();
	m_scriptThread->SetInputText(text);
	m_scriptThread->InitData(m_maxCount);
	for (int i = 1; i <= m_maxCount; i++)
	{
		m_scriptThread->SetInputValue(m_inputNames[i-1], m_inputValues[i-1], i);
	}
	//QStringList texts = readInputString(text);
	m_calculateThread.start();
	emit StartCalculate();
}

void Calculator::GetResult(const QJSValue& result, QStringList error)//计算结果
{
	ui.m_outPutList->clear();
	m_outputValues.clear();
	m_outputNames.clear();
	for (int j = 1; j <= m_maxCount; j++)
	{
		QString tempName = QString("_%1").arg(j);
		QStringList tempOutPutNames;
		QList<double> tempOutPutValues;
		ui.m_outPutList->addItem(QString(u8"第%1次结果：").arg(j));
		for (int i = 1; i < 100; i++)
		{
			QString propertyName = QString("OutPut_%1").arg(i)+ tempName;
			if (result.property(propertyName).toNumber() != -1)
			{
				double value = result.property(propertyName).toNumber();
				QString text = propertyName + QString("=%1").arg(value);
				ui.m_outPutList->addItem(text);
				tempOutPutValues.append(value);
				tempOutPutNames.append(propertyName.left(propertyName.size()- tempName.size()));
			}
		}
		m_outputValues.append(tempOutPutValues);
		m_outputNames.append(tempOutPutNames);
	}
	ExitThread();
	ui.m_outPutLog->clear();
	for (int i = 0; i < error.size(); i++)
	{
		ui.m_outPutLog->appendPlainText(u8"错误:" + error[i]);
	}
	CreateResultExcel();
}

void Calculator::ReadInputData()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr(u8"打开表格"), "", tr(u8"excel表格 (*.xlsx *.xls)"));
	QXlsx::Document openXlsx(fileName);
	if (openXlsx.load()) // load excel file
	{
		if (GetStringValueFromCell(openXlsx, 1, 1) != u8"变量名称")
		{
			return;
		}
		if (GetStringValueFromCell(openXlsx, 1, 2) != u8"变量数值")
		{
			return;
		}
		m_maxCount = 0;
		m_inputNames.clear();
		m_inputValues.clear();
		int maxRow = 0;
		for (int i = 1; i <= 100; i++)
		{
			bool isHave = false;
			QStringList tempInPutNames;
			QList<double> tempInPutValues;
			for (int j = 1; j <= 100; j++)
			{
				if (GetStringValueFromCell(openXlsx, j + 1, i + 1) != "")
				{
					isHave = true;
					tempInPutNames.append(QString("InPut_%1").arg(j));
					double value = GetDoubleValueFromCell(openXlsx, j + 1, i + 1);
					tempInPutValues.append(value);
				}
			}
			if (isHave)
			{
				m_inputNames.append(tempInPutNames);
				m_inputValues.append(tempInPutValues);
				if (maxRow < tempInPutNames.size())
				{
					maxRow = tempInPutNames.size();
				}
				m_maxCount++;
			}
		}
		ui.m_inputTable->clear();
		if (m_maxCount > 0)
		{
			QStringList allNames= FindAllName(m_inputNames);
			ui.m_inputTable->setRowCount(allNames.size());
			ui.m_inputTable->setColumnCount(m_maxCount+1);
			ui.m_inputTable->setHorizontalHeaderItem(0, new QTableWidgetItem(u8"变量名"));
			for (int i = 0; i < m_maxCount; i++)
			{
				ui.m_inputTable->setHorizontalHeaderItem(i+1, new QTableWidgetItem(u8"数值"));
			}
			for (int i = 0; i < allNames.size(); i++)
			{
				ui.m_inputTable->setItem(i, 0, new QTableWidgetItem(allNames[i]));
			}
			for (int i = 0; i < m_inputNames.size(); i++)
			{
				for (int j = 0; j < m_inputNames[i].size(); j++)
				{
					int index = allNames.indexOf(m_inputNames[i][j]);
					ui.m_inputTable->setItem(index,i+1,new QTableWidgetItem(QString::number(m_inputValues[i][j])));
				}
			}
		}
		
	}

}

void Calculator::CreateBaseDataFile()
{
	QString fileName = QFileDialog::getSaveFileName(this,
		tr(u8"生成表格"), "./temp.xlsx", tr(u8"excel表格 (*.xlsx *.xls)"));
	QXlsx::Document xlsx;
	xlsx.write("A1", u8"变量名称");
	xlsx.write("B1", u8"变量数值");
	for (int i = 1; i < 100; i++)
	{
		QString tempName = QString("A%1").arg(i + 1);
		QString tempNameValue = QString("InPut_%1").arg(i);
		xlsx.write(tempName, tempNameValue);
	}
	xlsx.saveAs(fileName);
}

void Calculator::InitThread()
{
	m_scriptThread = new MyQScript;
	m_scriptThread->moveToThread(&m_calculateThread);
	connect(this, &Calculator::StartCalculate, m_scriptThread, &MyQScript::Caluate);
	connect(m_scriptThread, &MyQScript::EmitResult, this, &Calculator::GetResult);
}

void Calculator::ExitThread()
{
	m_calculateThread.quit();
	m_calculateThread.wait();
}

void Calculator::CreateResultExcel()
{
	QString fileName = QFileDialog::getSaveFileName(this,
		tr(u8"生成输出表格"), "./result.xlsx", tr(u8"excel表格 (*.xlsx *.xls)"));
	QXlsx::Document xlsx;
	xlsx.write("A1", u8"变量名称");
	QStringList allNames = FindAllName(m_outputNames);
	for (int i = 0; i < allNames.size(); i++)
	{
		xlsx.write(i+2,1, allNames[i]);
	}
	for (int i = 0; i < m_outputNames.size(); i++)
	{
		xlsx.write(1, i+2, QString(u8"第%1次结果").arg(i+1));
	}

	for (int i = 0; i < m_outputNames.size(); i++)
	{
		for (int j = 0; j < m_outputNames[i].size(); j++)
		{
			int index = allNames.indexOf(m_outputNames[i][j]);
			xlsx.write(index+2, i + 2, m_outputValues[i][j]);
		}
	}
	xlsx.saveAs(fileName);
}

double Calculator::GetDoubleValueFromCell(QXlsx::Document& xlsx, int row, int col)
{
	QXlsx::Cell* cell = xlsx.cellAt(row, col); // get cell pointer.
	if (cell != NULL)
	{
		QVariant var = cell->readValue(); // read cell value (number(double), QDateTime, QString ...)
		return var.toDouble();
	}
	return 0;
}

QString Calculator::GetStringValueFromCell(QXlsx::Document& xlsx, int row, int col)
{
	QXlsx::Cell* cell = xlsx.cellAt(row, col); // get cell pointer.
	if (cell != NULL)
	{
		QVariant var = cell->readValue(); // read cell value (number(double), QDateTime, QString ...)
		return var.toString();
	}
	return QString();
}

QStringList Calculator::FindAllName(QList<QStringList> names)
{
	QStringList allNames;
	for (int i = 0; i < names.size(); i++)
	{
		for (int j = 0; j < names[i].size(); j++)
		{
			if (!allNames.contains(names[i][j]))
			{
				allNames.append(names[i][j]);
			}
		}
	}
	return allNames;
}
