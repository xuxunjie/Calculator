#include"..\include\MyThreadBase.h"
#include<QObject>
MyThreadBase::MyThreadBase()
{
	InitData();
}


MyThreadBase::~MyThreadBase()
{

}

void MyThreadBase::InitData()
{
	SetPauseType(false);
	m_exitLock.lockForWrite();
	m_bIsExit = false;
	m_exitLock.unlock();
}

void MyThreadBase::SetPauseType(bool isPause)
{
	m_pauseLock.lockForWrite();
	m_bIsPause = false;
	m_pauseLock.unlock();
}

bool MyThreadBase::GetPauseType()
{
	bool value = false;
	m_pauseLock.lockForRead();
	value=m_bIsPause;
	m_pauseLock.unlock();
	return value;
}

void MyThreadBase::ExitThread()
{
	m_exitLock.lockForWrite();
	m_bIsExit = true;
	m_exitLock.unlock();
}