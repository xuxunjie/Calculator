#include "..\include\MyQScriptSetting.h"
#include<qmath.h>
MyQScriptSetting::MyQScriptSetting(QObject* parent) :QObject(parent)
{
}


MyQScriptSetting::~MyQScriptSetting()
{

}

double MyQScriptSetting::GetDis(double x1, double y1, double x2, double y2)
{
	double dis = qSqrt(qPow(x1 - x2, 2) + qPow(y1 - y2, 2));
	return dis;
}
