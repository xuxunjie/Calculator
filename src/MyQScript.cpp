#include "..\include\MyQScript.h"

MyQScript::MyQScript()
{
	m_data = "";
	m_count = 1;
	InitData(1);
	ImportSetting();
}


MyQScript::~MyQScript()
{
}

void MyQScript::SetInputText(QString text)
{
	m_data = text;
}

void MyQScript::SetInputValue(QStringList names, QList<double> values, int count)
{
	if (names.size() != values.size())
		return;
	QString tempName = QString("_%1").arg(count);
	for (int i = 0; i < names.size(); i++)
	{
		m_jsEngine.globalObject().setProperty(names[i]+ tempName, values[i]);
	}
}

void MyQScript::Caluate()
{
	QStringList allError;
	for (int i = 1; i <= m_count; i++)
	{
		QString realData = ProcessText(m_data, i);
		QJSValue result = m_jsEngine.evaluate(realData);
		QString error = "";
		if (result.isError())
		{
			error = QString(u8"第%1次运行错误发生在第 %2行 :%3").arg(i).arg(result.property("lineNumber").toInt()).arg(result.toString());
		}		
		if (error != "")
		{
			allError.append(error);
		}
	}
	emit EmitResult(m_jsEngine.globalObject(), allError);
}

void MyQScript::ImportSetting()
{
	MyQScriptSetting* m_setting=new MyQScriptSetting(this);
	QJSValue jsObject = m_jsEngine.newQObject(m_setting);
	m_jsEngine.globalObject().setProperty("Setting", jsObject);
}

QString MyQScript::ProcessText(QString text, int count)
{
	QString result = text;
	for (int i = 1; i <= 100; i++)
	{
		QString findName = QString("OutPut_%1").arg(i);
		QString resultName= QString("OutPut_%1_%2").arg(i).arg(count);
		result= result.replace(findName, resultName);
	}
	for (int i = 1; i <= 100; i++)
	{
		QString findName = QString("InPut_%1").arg(i);
		QString resultName = QString("InPut_%1_%2").arg(i).arg(count);
		result = result.replace(findName, resultName);
	}
	return result;
}

void MyQScript::InitData(int count)
{
	m_count = count;
	for (int j = 1; j <= m_count; j++)
	{
		QString tempName= QString("_%1").arg(j);
		for (int i = 1; i < 100; i++)
		{
			QString propertyName = QString("OutPut_%1").arg(i)+ tempName;
			m_jsEngine.globalObject().setProperty(propertyName, -1);
		}
		for (int i = 1; i < 100; i++)
		{
			QString propertyName = QString("InPut_%1").arg(i)+ tempName;
			m_jsEngine.globalObject().setProperty(propertyName, -1);
		}
	}
	
}
