/*****************************************************************//**
 * \file   ThreadBase.h
 * \brief  线程基类
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once
#include<QObject>
#include<QReadWriteLock>
class MyThreadBase : public QObject
{
    Q_OBJECT

public:
    MyThreadBase();
    ~MyThreadBase();
    //初始化参数
    void InitData();
    //设置是否暂停
    void SetPauseType(bool isPause);
    //返回暂停状态
    bool GetPauseType();
    

private slots:
    void ExitThread();//退出线程
private:
    QReadWriteLock m_exitLock;
    QReadWriteLock m_pauseLock;
    bool m_bIsExit;//是否退出线程
    bool m_bIsPause;//是否暂停线程
};
