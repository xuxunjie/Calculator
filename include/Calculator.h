/*****************************************************************//**
 * \file   Calculator.h
 * \brief  
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once

#include <QtWidgets/QMainWindow>
#include"ui_Calculator.h"
#include"MyQScript.h"
#include<QThread>

#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"

class Calculator : public QMainWindow
{
    Q_OBJECT

public:
    Calculator(QWidget *parent = Q_NULLPTR);
    ~Calculator();
signals:
    void StartCalculate();//开始计算线程
private slots:
    void Calculate();  //计算结果
    void GetResult(const QJSValue& result, QStringList error);//获取结果
    void ReadInputData();//加载本地数据
    void CreateBaseDataFile();//创建基础文件
private:
    void InitThread();
    void ExitThread();
    void CreateResultExcel();
    //double -1 失败值  QString ""失败值
    double GetDoubleValueFromCell(QXlsx::Document&,int row, int col);
    QString GetStringValueFromCell(QXlsx::Document&, int row, int col);
    //找出所有不重复的名称
    QStringList FindAllName(QList<QStringList>names);
private:
    Ui::CalculatorClass ui;
    MyQScript* m_scriptThread;
    QThread  m_calculateThread;//计算线程
    QList<QStringList> m_inputNames;//输入的公式
    QList<QList<double>>m_inputValues;//输入 数据
    QList <QStringList> m_outputNames;//输入的公式
    QList<QList<double>>m_outputValues;//输出 数据
    int m_maxCount;//最大次数 如果到达最大次数时，其他变量无值时为默认值-1
};
