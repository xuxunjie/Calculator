/*****************************************************************//**
 * \file   MyQScript.h
 * \brief  脚本类 用于调用处理数据
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once
#include"MyThreadBase.h"
#include"MyQScriptSetting.h"
#include<QObject>
#include<QJSEngine>
class MyQScript : public MyThreadBase
{
    Q_OBJECT

public:
    MyQScript();
    ~MyQScript();
    void SetInputText(QString text);
    //count=1 为第一次执行数据 count>=1
    void SetInputValue(QStringList names, QList<double> values,int count);
    void InitData(int count);//初始化参数  count为执行次数
    void Caluate();//执行脚本
    void ImportSetting();//加载默认函数


signals:
    void EmitResult(const QJSValue& result,QStringList errors);
private:
    QString ProcessText(QString text,int count);//根据次数处理数据
private:
    QJSEngine m_jsEngine;
    QString m_data;
    int m_count;//运行次数
};
