/*****************************************************************//**
 * \file   MyQScriptSetting.h
 * \brief  脚本类 用于调用处理数据
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once
#include<QObject>
#include<QJSEngine>

class MyQScriptSetting : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE  MyQScriptSetting(QObject* parent = nullptr);
    ~MyQScriptSetting();
    Q_INVOKABLE double GetDis(double x1,double y1,double x2,double y2);//求两点间距离

private:
    
private:
  
};
