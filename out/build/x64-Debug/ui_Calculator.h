/********************************************************************************
** Form generated from reading UI file 'Calculator.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALCULATOR_H
#define UI_CALCULATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CalculatorClass
{
public:
    QWidget *centralWidget;
    QFormLayout *formLayout_2;
    QWidget *widget;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_2;
    QLabel *label_3;
    QPushButton *m_calculateButton;
    QListWidget *m_outPutList;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QPlainTextEdit *m_inputText;
    QGridLayout *gridLayout;
    QTableWidget *m_inputTable;
    QPushButton *m_createFileButton;
    QLabel *label_2;
    QPushButton *m_loadInputDataButton;
    QPlainTextEdit *m_outPutLog;

    void setupUi(QMainWindow *CalculatorClass)
    {
        if (CalculatorClass->objectName().isEmpty())
            CalculatorClass->setObjectName(QString::fromUtf8("CalculatorClass"));
        CalculatorClass->resize(965, 642);
        centralWidget = new QWidget(CalculatorClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        formLayout_2 = new QFormLayout(centralWidget);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setHorizontalSpacing(0);
        formLayout_2->setVerticalSpacing(0);
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(0, 30));
        gridLayout_4 = new QGridLayout(widget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 0, 0, 1, 1);

        m_calculateButton = new QPushButton(widget);
        m_calculateButton->setObjectName(QString::fromUtf8("m_calculateButton"));

        gridLayout_2->addWidget(m_calculateButton, 0, 1, 1, 1);

        m_outPutList = new QListWidget(widget);
        m_outPutList->setObjectName(QString::fromUtf8("m_outPutList"));

        gridLayout_2->addWidget(m_outPutList, 1, 0, 1, 2);


        gridLayout_4->addLayout(gridLayout_2, 1, 1, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        m_inputText = new QPlainTextEdit(widget);
        m_inputText->setObjectName(QString::fromUtf8("m_inputText"));

        gridLayout_3->addWidget(m_inputText, 0, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 2);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        m_inputTable = new QTableWidget(widget);
        if (m_inputTable->columnCount() < 2)
            m_inputTable->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        m_inputTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        m_inputTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        m_inputTable->setObjectName(QString::fromUtf8("m_inputTable"));

        gridLayout->addWidget(m_inputTable, 1, 0, 1, 2);

        m_createFileButton = new QPushButton(widget);
        m_createFileButton->setObjectName(QString::fromUtf8("m_createFileButton"));

        gridLayout->addWidget(m_createFileButton, 0, 1, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        m_loadInputDataButton = new QPushButton(widget);
        m_loadInputDataButton->setObjectName(QString::fromUtf8("m_loadInputDataButton"));

        gridLayout->addWidget(m_loadInputDataButton, 2, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout, 1, 0, 1, 1);

        m_outPutLog = new QPlainTextEdit(widget);
        m_outPutLog->setObjectName(QString::fromUtf8("m_outPutLog"));

        gridLayout_4->addWidget(m_outPutLog, 2, 0, 1, 2);

        gridLayout_4->setRowStretch(0, 6);
        gridLayout_4->setRowStretch(1, 6);
        gridLayout_4->setRowStretch(2, 2);
        gridLayout_4->setColumnStretch(0, 1);
        gridLayout_4->setColumnStretch(1, 1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, widget);

        CalculatorClass->setCentralWidget(centralWidget);

        retranslateUi(CalculatorClass);

        QMetaObject::connectSlotsByName(CalculatorClass);
    } // setupUi

    void retranslateUi(QMainWindow *CalculatorClass)
    {
        CalculatorClass->setWindowTitle(QCoreApplication::translate("CalculatorClass", "QtWidgetsApplication1", nullptr));
        label_3->setText(QCoreApplication::translate("CalculatorClass", "\350\276\223\345\207\272\345\217\230\351\207\217\357\274\232", nullptr));
        m_calculateButton->setText(QCoreApplication::translate("CalculatorClass", "\350\256\241\347\256\227", nullptr));
        label->setText(QCoreApplication::translate("CalculatorClass", "\345\205\254\345\274\217\357\274\232", nullptr));
        m_inputText->setPlainText(QCoreApplication::translate("CalculatorClass", "//\344\276\213\345\255\220\n"
"OutPut_1=Setting.GetDis(1,1,10,9);\n"
"OutPut_2=InPut_1;\n"
"OutPut_3=InPut_2;\n"
"OutPut_4=InPut_3;\n"
"OutPut_5=InPut_4;", nullptr));
        QTableWidgetItem *___qtablewidgetitem = m_inputTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("CalculatorClass", "\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = m_inputTable->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("CalculatorClass", "\346\225\260\345\200\274", nullptr));
        m_createFileButton->setText(QCoreApplication::translate("CalculatorClass", "\345\210\233\345\273\272\345\237\272\347\241\200\346\226\207\344\273\266", nullptr));
        label_2->setText(QCoreApplication::translate("CalculatorClass", "\350\276\223\345\205\245\345\217\230\351\207\217\357\274\232", nullptr));
        m_loadInputDataButton->setText(QCoreApplication::translate("CalculatorClass", "\345\212\240\350\275\275\350\276\223\345\205\245\346\225\260\346\215\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CalculatorClass: public Ui_CalculatorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALCULATOR_H
